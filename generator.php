<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.11.2020
 * Time: 9:35
 */

namespace Generator;


function generate(): array {
  $result = [];
  for($i = 0; $i < 5; $i++) {
    $result[$i] = random_int(1, 6);
  }

  return $result;
}

function combination_name_from_array(array $result): string {
  // По совместительству - переменная состояния конечного автомата.
  $name = '';
  // Для отслеживания переходов между состояниями конечного автомата.
  $name_is_changed = false;
  // Для отслеживания нарастающей последовательности (малый и большой стриты).
  $incrementing = 0;

  foreach($result as $i => $value) {
    if (0 === $i) {
      continue;
    }

    $prev = $result[$i - 1];

    if ($value === $prev) {
      $incrementing = 0;
      if (empty($name)) {
        $name = 'пара';
        $name_is_changed = true;
      }
      else {
        switch($name) {
          case 'пара':
            if ($name_is_changed) {
              $name = 'сэт';
            }
            else {
              $name = 'две пары';
            }
            $name_is_changed = true;
            break;

          case 'сэт':
            if ($name_is_changed) {
              $name = 'каре';
            }
            else {
              $name = 'фул хаус';
            }
            $name_is_changed = true;
            break;

          case 'каре':
            $name = 'покер';
            $name_is_changed = true;
            break;

          case 'две пары':
            $name = 'фул хаус';
            $name_is_changed = true;
            break;

          default:
            $name_is_changed = false;
            break;
        }
      }
    }
    elseif (1 === $value - $prev) {
      $incrementing++;
      if (3 === $incrementing) {
        $name = 'малый стрит';
        $name_is_changed = true;
      }
      elseif (4 === $incrementing) {
        $name = 'большой стрит';
        $name_is_changed = true;
      }
    }
    else {
      $incrementing = 0;
      $name_is_changed = false;
    }
  }

  if (empty($name)) {
    $name = 'шанс';
  }

  return $name;
}

