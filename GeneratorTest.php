<?php
namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use function Generator\combination_name_from_array;

class GeneratorTest extends TestCase {

  public function testChance() {
    $result = [1, 2, 4, 5, 6];
    $name = combination_name_from_array($result);
    self::assertEquals('шанс', $name);
  }

  public function testOnePair() {
    $result = [2, 2, 4, 5, 6];
    $name = combination_name_from_array($result);
    self::assertEquals('пара', $name);
  }

  public function testTwoPairs() {
    $result = [2, 2, 4, 4, 6];
    $name = combination_name_from_array($result);
    self::assertEquals('две пары', $name);

    $result = [2, 2, 5, 4, 4];
    $name = combination_name_from_array($result);
    self::assertEquals('две пары', $name);
  }

  public function testSet() {
    $result = [2, 2, 2, 4, 6];
    $name = combination_name_from_array($result);
    self::assertEquals('сэт', $name);

    $result = [1, 2, 3, 3, 3];
    $name = combination_name_from_array($result);
    self::assertEquals('сэт', $name);
  }

  public function testFourStreet() {
    $result = [1, 2, 3, 4, 6];
    $name = combination_name_from_array($result);
    self::assertEquals('малый стрит', $name);

    $result = [2, 3, 4, 5, 1];
    $name = combination_name_from_array($result);
    self::assertEquals('малый стрит', $name);

    $result = [1, 3, 4, 5, 6];
    $name = combination_name_from_array($result);
    self::assertEquals('малый стрит', $name);
  }

  public function testFifthStreet() {
    $result = [1, 2, 3, 4, 5];
    $name = combination_name_from_array($result);
    self::assertEquals('большой стрит', $name);

    $result = [2, 3, 4, 5, 6];
    $name = combination_name_from_array($result);
    self::assertEquals('большой стрит', $name);
  }

  public function testFullHouse() {
    $result = [1, 1, 3, 3, 3];
    $name = combination_name_from_array($result);
    self::assertEquals('фул хаус', $name);

    $result = [1, 1, 1, 3, 3];
    $name = combination_name_from_array($result);
    self::assertEquals('фул хаус', $name);
  }

  public function testFourOfAKind() {
    $result = [1, 3, 3, 3, 3];
    $name = combination_name_from_array($result);
    self::assertEquals('каре', $name);

    $result = [5, 5, 5, 5, 3];
    $name = combination_name_from_array($result);
    self::assertEquals('каре', $name);
  }

  public function testFiveOfAKind() {
    $result = [3, 3, 3, 3, 3];
    $name = combination_name_from_array($result);
    self::assertEquals('покер', $name);
  }
}
