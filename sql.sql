CREATE TABLE `transaction` (
	`id` TINYINT(3) UNSIGNED NOT NULL,
	`date` TINYINT(3) UNSIGNED NOT NULL,
	`amount` TINYINT(4) NOT NULL,
	UNIQUE INDEX `date_idx` (`date`) USING BTREE
)
	COLLATE='utf8_general_ci'
	ENGINE=InnoDB
;

INSERT INTO transaction (`id`, `date`, `amount`) VALUES
(1, 1, 1),
(2, 2, 2),
(6, 3, -1),
(8, 4, 2),
(7, 5, -6),
(9, 6, 1),
(3, 7, -5),
(4, 8, 1),
(5, 9, 7);

# Считаем нарастающие итоги.
# Выбираем из них date для первого отрицательного итога.
# По этой date берем id.
SELECT t.id
FROM transaction AS t
WHERE t.date = (
	SELECT MIN(totals.date)
	FROM (
		SELECT t.*,
			(SELECT COALESCE(SUM(t2.amount), 0) FROM transaction t2 WHERE t2.date <= t.date ) AS total
		FROM transaction AS t
		ORDER BY t.date) AS totals
	WHERE totals.total < 0
);
